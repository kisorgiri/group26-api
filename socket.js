const socket = require('socket.io');
var users = [];
module.exports = function (app, config) {
    const io = socket(app.listen(config.socketPort, function (err, done) {
        if (err) {
            console.log('error listening socket server');
        } else {
            console.log('socket server listening at port ' + config.socketPort)
        }
    }));
    io.on('connection', function (client) {
        var id = client.id;
        console.log('client connected to server');
        client.on('new-msg', function (data) {
            console.log('new-msg >>', data)
            client.emit('reply-msg-own', data); // for own client which has fired event
            client.broadcast.to(data.receiverId).emit('reply-msg', data); // for every other client except requesting one
        });
        client.on('new-user', function (data) {
            users.push({
                id,
                name: data
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        client.on('disconnect', function () {
            users.forEach(function (user, index) {
                if (user.id === id) {
                    users.splice(index, 1);
                }
            });
            client.broadcast.emit('users', users);
        })
    })

}