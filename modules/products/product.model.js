const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ReviewSchema = new Schema({
    point: Number,
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: String,
    description: String,
    price: Number,
    category: {
        type: String,
        required: true
    },
    color: String,
    brand: String,
    size: String,
    images: [String],
    condition: {
        type: String,
        enum: ['brand new', 'used'],
        default: 'brand new'
    },
    status: {
        type: String,
        enum: ['available', 'out of stock', 'booked'],
        default: 'available'
    },
    modelNo: String,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    warrentyStatus: Boolean,
    warrentyPeroid: String,
    discount: {
        discountedItem: Boolean,
        discountType: String,
        discountValue: String
    },
    tags: [String],
    reviews: [ReviewSchema],
    cuponCode: String,
    offers: [String]
}, {
    timestamps: true
});

module.exports = mongoose.model('product', ProductSchema);

