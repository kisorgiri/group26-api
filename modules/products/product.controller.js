const ProductQuery = require('./product.query');
const fs = require('fs');
const path = require('path');


function get(req, res, next) {
    const condition = {};
    if (req.user.role !== 1) {
        condition.vendor = req.user._id;
    }
    ProductQuery.find(condition)
        .then(function (data) {
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })
}

function getById(req, res, next) {
    const condition = {
        _id: req.params.id
    }
    ProductQuery.find(condition)
        .then(function (data) {
            res.status(200).json(data[0])
        })
        .catch(function (err) {
            next(err)
        })
}

function insert(req, res, next) {
    console.log('req.body>>', req.body);
    console.log('req.file >>', req.file)
    console.log('req.files >>', req.files)
    if (req.fileError) {
        return next({
            msg: req.fileError,
            status: 400
        })
    }
    // data preparation
    const data = req.body;
    if (req.file) {
        data.images = req.file.filename
    }
    if (req.files) {
        data.images = req.files.map(function (file, i) {
            return file.filename
        })
    }
    // image
    // user
    req.body.vendor = req.user._id;
    ProductQuery
        .save(data)
        .then(function (data) {
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })
}

function update(req, res, next) {
    const data = req.body;
    data.user = req.user._id;
    if (req.fileError) {
        return next({
            msg: req.fileError,
            status: 400
        })
    }
    if (req.file) {
        data.images = req.file.filename;
    }
    if (req.files) {
        data.images = req.files.map(function (file, i) {
            return file.filename
        })
    }
    const productId = req.params.id;
    ProductQuery.update(productId, data)
        .then(function (data) {
            if (req.file) {
                fs.unlink(path.join(process.cwd(), 'uploads/images/' + data.oldImage), function (err, done) {
                    if (!err) {
                        console.log('file removed');
                    }
                })
            }
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })

}

function remove(req, res, next) {
    ProductQuery.remove(req.params.id)
        .then(function (data) {
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })

}

function search(req, res, next) {
    const searchCondition = {} // searchCondition
    ProductQuery.mapProductReq(searchCondition, req.body);
    if (req.body.minPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice,
            $gte: req.body.minPrice
        }
    }
    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);
        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    console.log('search condition >', searchCondition);
    ProductQuery.find(searchCondition)
        .then(function (data) {
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })
}

module.exports = {
    get,
    getById,
    insert,
    update,
    remove,
    search
}