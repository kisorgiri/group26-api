const ProductCtrl = require('./product.controller');
const Authenticate = require('./../../middlewares/authenticate');
const upload = require('./../../middlewares/uploader');


const router = require('express').Router();
router.route('/')
    .get(Authenticate, ProductCtrl.get)
    .post(upload.array('img'), Authenticate, ProductCtrl.insert)

router.route('/search')
    .get(ProductCtrl.search)
    .post(ProductCtrl.search);

router.route('/:id')
    .get(Authenticate, ProductCtrl.getById)
    .put(upload.array('img'), Authenticate, ProductCtrl.update)
    .delete(Authenticate, ProductCtrl.remove);

module.exports = router;