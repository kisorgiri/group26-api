const http = require('http');
const fileOp = require('./abc');
const server = http.createServer(function (request, response) {
    // this is a callback for handling each client request
    console.log('client connected to server');
    // request or 1st argument is http request object
    // response or 2nd argument is http response object
    console.log('req.method>>', request.method)
    console.log('req.=url>>', request.url)
    if (request.url === '/write') {
        fileOp.write('kishor.txt', 'welcome')
            .then(function (data) {
                response.end('success in write')
            })
            .catch(function(err){
                response.end('failure in write')
            })

    } else if (request.url === '/read') {

    } else if (request.url === '/rename') {

    } else if (request.url === '/remove') {

    } else {
        response.end('No any action to perform');
    }

    // req-res cycle must be completed
    // for each request a single response must be sent

    // regardless of url and method this callback block is executed for each client request

});

server.listen(9090, function (err, done) {
    if (err) {
        console.log('error ', err);
    } else {
        console.log('server listening at port 9090')
        console.log('press CTRL + C to exit');
    }
});


// protocol http
// http==> verb(methods)==> GET,PUT,POST,DELETE,PATCH,......
// http status code 