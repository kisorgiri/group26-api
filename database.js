// database is a container where data operation are performed (create read update delete);

// the way of working with database is database management system
// two types of database management system
// 1 Relational Database management system
// 2 distributed DBMS

// 1 relational database management system
// a. table based design ==>
// eg LMS books,users are table
// ==>tuple, row
// b Schema based solution (database modelling) // table >> attributes>> relations
// schema is a way of defining the property for each records(tuple)
// c.non scalable ==> we cannot add any new property other then defined in schema
// d. relation between table exist
// e.also called as SQL (structured Query Language)
// f. types=> mysqlm mssql, oracle, 

// 2.Distributed Database Management System
// a. collections based design
// ==> eg. LMS books,users are collections
// ===> document ==> document based database
// document is a valid json object
// b. Schema less solution.
// c highly scalable database
// d there is no any relation between collections
// e. also called as NOSQL (not only sql)
// f. types ==> mongodb, couchdb, dynamodb,redis


// mongodb in our system
// install
// add mongodb path in environment >> system variables >> path
// after adding in the path we can access mongodb from any location of our computer
// eg node, nodemon, express

// mongodb database from command line
// mongoshell
// to access a mongoshell  use mongo command ( followed by mongod)
// we will get > iterface once connection is established

// command
// show dbs ==> list all the available databases
// use <db_name> // if db exist it will select it else it will create a new database and select
// db ==> shows the selected database
// show collections ==> list the available collections of selected database

// CRUD
// Create
// db.<collection_name>.insert({valid_json_object});
// db.<collection_name>.insertMany(Array);

// Read
// db.<collection_name>.find({query_selector})
// db.<collection_name>.find({query_selector}).count(); // return count
// db.<collection_name>.find({query_selector}).pretty() // format output
// db.<collection_name>.find({query_selector}).other helper query(eg.sort,limit,skip) // format output

// Update
// db.<collection_name>.update({},{},{})
// 1st object is query builder
// 2nd object must have $set as an key value is object(data) that needs to be updated,
// 3rd object is optional and it will hold options

// Delete
// db.<collection_name>.remove({query_builder});
// note don't leave the remove query builder empty


// advantages of ODM
// mongoose for nodejs with mongodb

// 1. SCHEMA Based solution ==> db modelling always give insight about application
// 2. indexing are lot more easier ==> required,unique,
// 3. data type are defined,
// 4. own methods,
// 5. middlewares 

//############### BACKUP & RESTORE#######################################
// 2 format
// machine readble and human readble
//(bson and (json and csv))
// 1.bson
// command mongodump for backup and mongorestore for restore
// backup
// mongodump ==> it will backup entire database on default dump folder
// mongodump --db <db_name> it will backup selected database on default dump folder
// mongodump --db <db_name> --out <output directory path>

// restore
// mongorestore  ==> it checks for dump folder and try to restore the database
// mongorestore --drop ==> existing db drop
// mongorestore <path_to_existing_backup db>

// JSON and CSV
// command  mongoimport and mongoexport
// 1 json
// backup 
// mongoexport --db <db_name> --collection <col_name> --out <path_to_out_dir_with_.json_extension>
// mongoexport -d <db_name> -c <col_name> -o <path_to_out_dir_with_.json_extension>

// restore
// mongoimport --db<db_name> --collection<col_name> <path_to_source_file>
// csv
// backup
// mongoexport --db <db_name> --collection <col_name> --type='csv' --fields 'comma seperated value to be exported' --out <path to destination with .csv extension>
// mongoexport --db <db_name> --collection <col_name> --type='csv' --query="{key:'value'}" --fields 'comma seperated value to be exported' --out <path to destination with .csv extension>

// restore
// mongoimport --db <db_name> --collection <colName> --type=csv <path_to_csv_file> --headerline

//############### BACKUP & RESTORE#######################################


