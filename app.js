const express = require('express');
const morgan = require('morgan');
const app = express();
const path = require('path');
const cors = require('cors');
const config = require('./configs');
require('./db');

// load routing level middleware
const APIRoute = require('./routes/api.route');

// socket stuff
require('./socket')(app, config);

// third party middleware
app.use(morgan('dev'));
app.use(cors());

// inbuilt middleware for serving static files
app.use(express.static('uploads')) // internal usage within express application
app.use('/file', express.static(path.join(__dirname, 'uploads')));
// parse incoming data
// for x-www-formurlencoded
app.use(express.urlencoded({ extended: true }));
// for json
app.use(express.json());

// event stuff
const events = require('events');
const myEvent = new events.EventEmitter();
app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

myEvent.on('myError', function (error, res) {
    console.log('error at my own middleware', error);
    res.status(400).json(error);
})

// use routing level level middleware
// mount all incoming request as per url
app.use('/api', APIRoute);

app.use(function (req, res, next) {
    // 404 catch block
    next({
        msg: "Not Found",
        status: 404
    })
})

// middleware with 4 argument is error handling middleware
// 1st argument is for errors
// 2,3,4 are req,res,next
// calling a next with argument triggers error handling middleware
// the argument passed in next is accisbile in 1st placeholder of error handling middleware
app.use(function (err, req, res, next) {
    console.log('error handling middleware', err);
    res.status(400)
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})



app.listen(config.port, function (err, done) {
    if (err) {
        console.log('error listening  >', err);
    } else {
        console.log("Server is listening at port  " + config.port);

    }
})


// middleware
// middleware is a function which came into action in between req-res cycle
// middleware has three arguments for http req, http res and next middleware function reference
// middleware can access http request object http response object and can modify them
// middlewares order is very very important

// syntax
// function(req,res,next){
//     // req or 1st argument is http request object
//     // res or 2nd argument is http response object
//     // next is reference of next middleware function
// }

// configuration block(use method)

// app.use() is configuration block for middleware
// example

// types of middleware
// 1. application level middleware
// 2. routing level middleware
// 3 third party middleware
// 4 inbuild middleware
// 5 error handling middleware

// 1 application level middleware
// a middleware having a scope of req,res and next is application level middleware