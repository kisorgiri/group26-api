const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    // db modelling
    name: String,
    phoneNumber: Number,
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    address: {
        temp_addr: [String],
        permanent_addr: String
    },
    dob: Date,
    gender: {
        type: String,
        enum: ['male', 'female', 'others'],
    },
    country: String,
    role: {
        type: Number, //1 for admin,2 for enduser,
        default: 2
    },
    status: {
        type: String,
        default: 'active'
    },
    passwordResetExpiry: Date
})

const UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;