const path = require('path');

const multer = require('multer');

// const upload = multer({
//     dest: path.join(process.cwd(), 'uploads')
// })

function filter(req, file, cb) {
    const mimetype = file.mimetype.split('/')[0];
    if (mimetype === 'image') {
        cb(null, true)
    } else {
        req.fileError = 'Invalid File Format'
        cb(null, false)
    }
}

const diskStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    },
    destination: function (req, file, cb) {
        cb(null, path.join(process.cwd(), 'uploads/images'))
    }
})
const upload = multer({
    storage: diskStorage,
    fileFilter: filter
})

module.exports = upload;