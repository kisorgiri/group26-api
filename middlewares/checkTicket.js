module.exports = function (req, res, next) {

    if (req.query.ticket === 'ram') {
        next();
    } else {
        next({
            msg: "Invalid Ticket",
            status: 400
        })
    }
}