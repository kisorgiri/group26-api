const jwt = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    var token;
    if (req.headers['authorization'])
        token = req.headers['authorization'];
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.headers['token'])
        token = req.headers['token']
    if (req.query.token)
        token = req.query.token;

    if (token) {
        jwt.verify(token, config.jwtSecret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            console.log('decoded value is >', decoded);
            UserModel.findById({ _id: decoded._id })
                .then(function (user) {
                    if (user) { 
                        // database current record is attached in every req 
                        req.user = user;
                        next();
                    } else {
                        next({
                            msg: "User removed from system"
                        })
                    }
                })

        })
    }
    else {
        next({
            msg: "Token Not Provided",
            status: 400
        })
    }
}

// task prepare a authorize middleware
// description
// if admin then allow req res to be completed
// else break the cycle with message you dont have permisision