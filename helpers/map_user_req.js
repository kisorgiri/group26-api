module.exports = function (user, userInfo) {
    if (userInfo.name)
        user.name = userInfo.name;
    if (userInfo.email)
        user.email = userInfo.email;
    if (userInfo.username)
        user.username = userInfo.username;
    if (userInfo.password)
        user.password = userInfo.password;
    if (userInfo.phoneNumber)
        user.phoneNumber = userInfo.phoneNumber;
    if (userInfo.gender)
        user.gender = userInfo.gender;
    if (userInfo.date_of_birth)
        user.dob = userInfo.date_of_birth;
    if (userInfo.role)
        user.role = userInfo.role
    if (userInfo.status)
        user.status = userInfo.status
    if (userInfo.gender)
        user.gender = userInfo.gender
    if (!user.address) {
        user.address = {}
    }
    if (userInfo.temp_addr)
        user.address.temp_addr = userInfo.temp_addr.split(',');
    if (userInfo.permanent_addr)
        user.address.permanent_addr = userInfo.permanent_addr

    return user;
}