var fruits = ['apple', 'mango', 'banana'];

var vegitables = ['ginger', 'tomato'];


// export syntax (es5)
// module.exports 

module.exports = {
    fruits, //es6 object shorthand notation
    vegitables: vegitables
}
