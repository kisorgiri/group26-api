const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function prepareMail(data) {
    return {
        from: '"Group 26 Store" <noreply@something.com>', // sender address
        to: 'kisorgiri@gmail.com,' + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Forgot Password?", // plain text body
        html: `
        <p>Hello <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having a trouble logging into our system. please use link below to reset your password.</p>
        <p><a href="${data.link}">click here to reset password</a></p>
        <p>If you have not requested to reset your password kindly ignore this email.</p>
        <p>Regards,</p>
        <p>Group 26 Support Team</p>
        `, // html body
    }
}

function createToken(data) {
    let token = jwt.sign({ _id: data._id }, config.jwtSecret);
    return token;
}

router.post('/login', function (req, res, next) {
    UserModel.findOne({
        $or: [
            { username: req.body.username },
            { email: req.body.username }
        ]
    })
        .then(function (user) {
            if (user) {
                const isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {
                    var token = createToken(user);
                    res.json({
                        user,
                        token
                    });
                } else {
                    next({
                        msg: 'invalid password',
                        status: 400
                    })
                }
            } else {
                next({
                    msg: "invalid username",
                    status: 400
                })
            }
        })
        .catch(function (err) {
            next(err);
        })
})
router.post('/register', function (req, res, next) {
    console.log('req.body >>', req.body);
    const newUser = new UserModel({});
    // newUser is mongoose object
    const newMappedUser = MapUser(newUser, req.body)

    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done);
    })
})

router.post('/forgot-password', function (req, res, next) {
    var email = req.body.email;
    UserModel
        .findOne({
            email: email
        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Email not registered",
                    status: 404
                })
            }
            // email stuff
            var data = {
                name: user.username,
                email,
                link: `${req.headers.origin}/reset_password/${user._id}`
            }
            const emailBody = prepareMail(data);

            user.passwordResetExpiry = new Date().getTime() + (1000 * 60 * 60 * 2);
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(emailBody, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
            })

        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/reset-password/:id', function (req, res, next) {
    var id = req.params.id;
    UserModel.findOne({
        _id: id,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Password reset link expired",
                    status: 400
                })
            }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetExpiry = null;

            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
})

router.get('/test-endpoint', function (req, res, next) {
    const fs = require('fs');
    fs.readFile('.sdfjl.sdl', function (err, done) {
        if (err) {
            req.myEvent.emit('myError', err, res);
        }
    })
})

module.exports = router;