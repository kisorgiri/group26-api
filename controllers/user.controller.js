const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');

router.route('/')
    .get(function (req, res, next) {
        UserModel
            .find({})
            .sort({
                _id: -1
            })
            .limit(2)
            .skip(1)
            .exec(function (err, result) {
                if (err) {
                    return next(err);
                }
                res.json(result);
            })

    })
    .post(function (req, res, next) {

    });



router.route('/profile')
    .get(function (req, res, next) {
    })
    .post(function (req, res, next) {
        res.json({
            msg: "from post of profile"
        })
    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/:id')// dynamic
    .get(function (req, res, next) {
        // fetch by id
        UserModel.findOne({ _id: req.params.id }, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                res.json(user);
            }
            if (!user) {
                next({
                    msg: "User Not Found",
                    status: 404
                })
            }
        })

    })
    .put(function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            var mappedUpdatedUser = MapUser(user, req.body);

            mappedUpdatedUser.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })

    })
    .delete(function (req, res, next) {
        if (req.user.role !== 1) {
            return next({
                msg: 'you dont have permission',
                status: 403
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            user.remove(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done)
            })
        })
    });



module.exports = router;