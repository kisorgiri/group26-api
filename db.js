const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');
const conxnURL = dbConfig.conxnURL + '/' + dbConfig.dbName;

mongoose.connect(conxnURL, { useNewUrlParser: true, useUnifiedTopology: true })
    ;

mongoose.connection.once('open', function () {
    console.log('db connection established');
})
mongoose.connection.on('error', function (err) {
    console.log('error connecting to db');
})