const router = require('express').Router();
const AuthRoute = require('./../controllers/auth.controller');
const UserRoute = require('./../controllers/user.controller');
const ProductRoute = require('./../modules/products/product.route');
const Authenticate = require('./../middlewares/authenticate');

router.use('/auth', AuthRoute)
router.use('/user', Authenticate, UserRoute)
router.use('/product', ProductRoute)


module.exports = router;